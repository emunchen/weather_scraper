# -*- coding: utf-8 -*-
from utils import scrape_table
from utils import get_city
from utils import save_data
from utils import slugify
import os
import pymongo

mongo_uri = "mongodb://%s:%s@%s/%s?authSource=admin" % (os.getenv('DB_USER', 'root'), os.getenv(
    'DB_PASSWORD', 'example'), os.getenv('DB_HOST', 'localhost'), os.getenv('DB_NAME', 'weather'))
client = pymongo.MongoClient(mongo_uri)
db = client[os.getenv('DB_NAME', 'weather')]


def main():
    result = scrape_table()
    cities = []
    for city in result:
        city_name = get_city(city)
        city.update({'name': city_name, 'slug': slugify(city_name)})
        cities.append(city)
    save_data(db, cities)


if __name__ == '__main__':
    main()
