config = {
    'web_to_scrape': 'https://app.deta.sh/hw6g4zdvlmao/',
    'coordinates_app': 'https://app.deta.sh/hw6g4zdvlmao/lookup?',
    'non_url_safe': ['"', '#', '$', '%', '&', '+', ',', '/', ':', ';', '=', '?', '@', '[', '\\', ']', '^', '`', '{', '|', '}', '~', "'"]
}
