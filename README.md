# Weather Scraper

These instructions are to install Weather Scraper

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Clone this repo

```
git clone git@bitbucket.org:emunchen/weather_scraper.git
```

What things you need to install the software and how to install them

```
sudo apt-get install python-virtualenv
```

Install pip

```
sudo apt-get install python-pip
```

### Installing

A step by step series of examples that tell you have to get a development env running

First create a virtual environment

```
cd weather_scraper && virtualenv venv

```

Activate your virtual environment

```

source venv/bin/activate

```

Install req.txt file

```

pip install -r req.txt

```

## Running

Run the main script.

```
python main.py
```

## Mongo DB

You can create a local Mongo DB using the `stack.yml` file on this repo with Docker Compose. Just run:

```
docker-compose -f stack.yml up
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [Git](https://git-scm.com/) for versioning.

## Authors

- **Emanuel Pecora** - _Initial work_ - [Emunchen](https://github.com/emunchen)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

