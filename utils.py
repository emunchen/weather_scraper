# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import pymongo
from config import config


def get_coordenates(input=None):

    latitude = input[0].split(' ')
    longitude = input[1].strip().split(' ')

    coordenates = {
        'latitude': {
            'value': latitude[0],
            'orientation': latitude[1].lower()
        },
        'longitude': {
            'value': longitude[0],
            'orientation': longitude[1].lower()
        }
    }

    return coordenates


def get_wind(wind=None):
    if wind is None:
        return
    wind = wind[0].split(' ')[0]
    return {'wind': wind}


def get_city(coordenates=None):
    query = {
        coordenates['latitude']['orientation']: coordenates['latitude']['value'],
        coordenates['longitude']['orientation']: coordenates['longitude']['value'],
    }
    url = config['coordinates_app']

    response = requests.get(url, params=query)
    return response.json()['result']


def slugify(text):
    """
    Turn the text content of a header into a slug for use in an ID
    """
    non_url_safe = config['non_url_safe']
    non_safe = [c for c in text if c in non_url_safe]
    if non_safe:
        for c in non_safe:
            text = text.replace(c, '')
    # Strip leading, trailing and multiple whitespace, convert remaining whitespace to _
    text = u'_'.join(text.split()).lower()
    return text


def scrape_table():
    url = config['web_to_scrape']
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    tb = soup.find('table')
    scrape_result = []
    for tr in tb.find_all('tr')[1:]:
        tds = tr.find_all('td')
        for td in tds:
            line = str(td).replace('<td>', '').replace(
                '</td>', '').replace('°', '').split(',')

            if len(line) == 2:
                coordenates = get_coordenates(line)
            else:
                wind = get_wind(line)

        coordenates.update(wind)
        scrape_result.append(coordenates)
    return scrape_result


def save_data(db, data):

    if db is None:
        return

    for row in data:
        db.cities.update({'slug': row['slug']}, row, True)
        print "Saving data for: %s" % row[u'name']
